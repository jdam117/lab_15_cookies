// Setup code
// --------------------------------------------------------------------------
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use body parser (for reading submitted form data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // Allows us to read forms submitted with POST requests

// Specify that the app should use express-session to create in-memory sessions
var session = require('express-session');
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: "compsci719"
}));
// --------------------------------------------------------------------------


// Passport setup code
// --------------------------------------------------------------------------

// Specify that the app should use "passport" for authentication, and
// that the authentication type should be "local".
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// Load user data from a file
var users = require("./example-03-users.json");

// A function to get a user with the given username from the "users" data.
// Will return the required user, or undefined.
function findUser(username) {
    return users.find(function (user) {
        return user.username == username;
    });
}

// Set up local authentication
var localStrategy = new LocalStrategy(
    function (username, password, done) {

        // Get the user from the "database"
        user = findUser(username);

        // If the user doesn't exist...
        if (!user) {
            return done(null, false, { message: 'Invalid user' });
        };

        // If the user's password doesn't match the typed password...
        if (user.password !== password) {
            return done(null, false, { message: 'Invalid password' });
        };

        // If we get here, everything's OK.
        done(null, user);
    }
);

// This method will be called when we need to save the currently
// authenticated user's username to the session.
passport.serializeUser(function (user, done) {
    done(null, user.username);
});

// This method will be called when we need to get all the data relating
// to the user with the given username.
passport.deserializeUser(function (username, done) {
    user = findUser(username);
    done(null, user);
});

// Set up Passport to use the given local authentication strategy
// we've defined above.
passport.use('local', localStrategy);

// Start up Passport, and tell it to use sessions to store necessary data.
app.use(passport.initialize());
app.use(passport.session());

// A helper function, which will check if there's a user that's logged in.
// If there is an authenticated user, then "next()" will be called,
// which will forward the request onto the actual page to be displayed.
// If there is no authenticated user, then they'll be redirected to
// the homepage.
// See below for an example of how to use this function.
function isLoggedIn(req, res, next) {
    // if user is authenticated, carry on 
    if (req.isAuthenticated()) {
        return next();
    }

    // redirect them to the home page
    res.redirect("/");
}
// --------------------------------------------------------------------------


// Route handlers
// --------------------------------------------------------------------------

// If the user navigates to "/" or "/login", if the user is already authenticated,
// redirect them to the /welcome page. Otherwise, render the login page.
app.get(["/", "/login"], function (req, res) {
    if (req.isAuthenticated()) {
        res.redirect("/welcome");
    }
    else {
        // This data will be used to display helpful error / info panels on the homepage.
        var data = {
            loggedOut: req.query.loggedOut, // Will be set when the user is redirected here upon logging out successfully.
            loginFail: req.query.loginFail // Will be set when the user is redirected here upon failing to login.
        }
        res.render("example-03-login", data);
    }
});

// If the user POSTs to "/login", process their authentication attempt. If it succeeds,
// redirect them to "/welcome". If it fails, redirect them to the homepage.
app.post('/login', passport.authenticate('local',
    {
        successRedirect: '/welcome',
        failureRedirect: '/?loginFail=true'
    }
));

// If the user navigates to "/logout", log them out and then redirect them
// to the homepage.
app.get("/logout", function (req, res) {
    req.logout();
    res.redirect("/?loggedOut=true");
});

// If the user navigates to "/welcome", if the user is not authenticated,
// redirect them to the login page (handled by isLoggedIn). Otherwise,
// render the welcome page, which displays the user's name.
app.get("/welcome", isLoggedIn, function (req, res) {

    res.render("example-03-welcome", { username: req.user.username });
});

// --------------------------------------------------------------------------


// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});